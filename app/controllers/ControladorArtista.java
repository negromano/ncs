package controllers;

import models.Artista;
import models.Cancion;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.artista.*;
import views.html.index;

import javax.inject.Inject;
import java.util.List;

//Clase Controlador de la clase modelo Artista
public class ControladorArtista extends Controller {

    @Inject
    FormFactory defaultForm;

    //Método para generar la vista que pide al nuevo artista sus datos para registrarse
    public Result agregar() {
        Form<Artista> artistaForm = defaultForm.form(Artista.class);
        return ok(agregarArtista.render(artistaForm));
    }

    //Se busca al usuario por el id, se carga la vista con sus datos para que el usuario modifique los deseados
    public Result editar(String usuario) {
        Artista artista = Artista.finder.byId(usuario);
        Form<Artista> artistaForm = defaultForm.form(Artista.class).fill(artista);
        return ok(modificarArtista.render(artistaForm));
    }

    //Muestra todos los artistas registrados
    public Result mostrarTodos() {
        List<Artista> lista = Artista.finder.all();
        return ok(mostrarTodos.render(lista));
    }

    //Confirmación previa a la eliminación del perfil
    public Result confirmacion(String usuario){
        return ok(eliminarArtista.render(usuario));
    }

    //Elimina al artista identificado con el usuario dado por parámetro
    public Result eliminar(String usuario) {

        Artista artista = Artista.finder.byId(usuario);
        List<Cancion> canciones = Cancion.finder.query().where().ilike("artista", artista.getUsuario()).findList();
        for(Cancion c:canciones){
            c.delete();
        }
        artista.delete();
        return ok(index.render());

    }

    //Se leen los valores del form, se verifica su información, si no hay se genera el nuevo artista y se guarda
    public Result guardar() {
        Form<Artista> formArtista = defaultForm.form(Artista.class).bindFromRequest();


        if(formArtista.hasErrors()){
            flash("danger","Error en el valor de los datos");
            return badRequest(agregarArtista.render(formArtista));
        }else {
            Artista artista = formArtista.get();
            if(Artista.finder.byId(artista.getUsuario().toLowerCase())!=null){
                flash("danger","Ya existe ese usuario");
                return ok(agregarArtista.render(formArtista));
            }
            //Ocurría un Bug con el correo, el Form no era capaz de obtenerlo
            String correo = formArtista.field("Correo").getValue().get();
            artista.setCorreoElectronico(correo);
            artista.save();
            return ok(index.render());
        }
    }

    //Se obtienen los datos del form y se actualiza al artista
    public Result actualizar() {
        Form<Artista> formArtista = defaultForm.form(Artista.class).bindFromRequest();
        if(formArtista.hasErrors()){
            flash("danger","Datos no válidos");
            return ok(modificarArtista.render(formArtista));
        }else {
            Artista artista = formArtista.get();
            Artista temp = Artista.finder.byId(artista.getUsuario());
            temp.setCorreoElectronico(artista.getCorreoElectronico());
            temp.setEdad(artista.getEdad());
            temp.setNombre(artista.getNombre());
            temp.setPass(artista.getPass());
            temp.setUsuario(artista.getUsuario());
            temp.update();
            return ok(index.render());
        }
    }

    //Login que carga el form y la vista para recibir el usuario y la contraseña
    public Result login() {
        Form<Artista> artistaForm = defaultForm.form(Artista.class);
        return ok(loginArtista.render(artistaForm));
    }

    //Del Login se obtiene el usuario y la contraseña, se busca al Usuario y se verifica que la contraseña sea la correcta
    public Result mostrarArtista() {
        Form<Artista> formArtista= defaultForm.form(Artista.class).bindFromRequest();
        String user = formArtista.field("Usuario").getValue().get();
        String pass = formArtista.field("Pass").getValue().get();
        Artista artista = Artista.finder.byId(user);
        if (artista == null) {
            flash("danger","No se encontró al artista");
            return ok(loginArtista.render(formArtista));
        } else if (artista.getPass().equalsIgnoreCase(pass)) {
            List<Cancion> canciones = Cancion.finder.query().where().ilike("artista", artista.getUsuario()).findList();
            return ok(consultaArtista.render(artista, canciones));
        } else {
            flash("danger","Contraseña equivocada");
            return ok(loginArtista.render(formArtista));
        }
    }

    //Búsqueda de un artista existente pero sin los derechos a editar
    public Result buscarArtista(){
        Form<Artista> formArtista= defaultForm.form(Artista.class).bindFromRequest();
        String usuario = formArtista.field("Usuario").getValue().get();
        if(usuario.equalsIgnoreCase("") || usuario.equals(null)){
            flash("danger","Nombre inválido");
            return ok(buscarArtista.render(formArtista));
        }
        Artista artista = Artista.finder.byId(usuario);
        if(artista == (null)){
            flash("danger","No existe un Artista con ese nombre");
            return ok(buscarArtista.render(formArtista));
        }
        List<Cancion> canciones = Cancion.finder.query().where().ilike("artista", artista.getUsuario()).findList();
        return ok(mostrarArtista.render(artista, canciones));
    }

}
