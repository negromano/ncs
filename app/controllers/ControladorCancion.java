package controllers;

import com.cloudinary.utils.ObjectUtils;
import io.ebean.Ebean;
import models.Artista;
import models.Cancion;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.cloudinary.*;
import views.html.cancion.agregarCancion;
import views.html.index;
import views.html.artista.consultaArtista;
import views.html.layout;
import views.html.cancion.*;

import javax.inject.Inject;

public class ControladorCancion extends Controller {

    //Archivo Cloudinary para subir los archivos a la nube
    private static Cloudinary cloud;

    @Inject
    private FormFactory defaultForm;

    //Método que muestra una canción
    public Result mostrarCancion(Integer id) {
        Cancion cancion = Cancion.finder.byId(id);
        return ok(consultaCancion.render(cancion));
    }

    //Elimina una canción y refresca la vista
    public Result eliminarCancion(Integer id) {
        Cancion c = Cancion.finder.byId(id);
        Artista a = Artista.finder.byId(c.getArtista());
        c.delete();
        List<Cancion> canciones = Cancion.finder.query().where().ilike("artista", a.getUsuario()).findList();
        return ok(consultaArtista.render(a, canciones));
    }

    //Sube una canción en formato mp3
    public Result upload(String artista) {
        Http.MultipartFormData<File> form = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> audioFile = form.getFile("Archivo de Audio");
        DynamicForm df = defaultForm.form().bindFromRequest();
        String nombre = df.get("nombre");
        String genero = df.get("genero");
        if (audioFile != null){
            File archivo = audioFile.getFile();
            String url = alaNube(archivo);
            Random r = new Random();
            Integer id = r.nextInt(Integer.MAX_VALUE);
            while (Cancion.finder.byId(id) != null) {
                id = r.nextInt(Integer.MAX_VALUE);
            }
            Cancion c = new Cancion(nombre, genero, id, artista, url);
            c.save();
            Artista a = Artista.finder.byId(artista);
            List<Cancion> canciones = Cancion.finder.query().where().ilike("artista", artista).findList();
            return ok(consultaArtista.render(a, canciones));
        }else{
            flash("error", "No se encontró el archivo");
            return badRequest("Error en la carga");
        }
    }

    //Agrega la canción al artista
    public Result agregarCancion(String artista) {
        Form<Cancion> form = defaultForm.form(Cancion.class);
        return ok(agregarCancion.render(form, artista));
    }

    //Sube el archivo mp3 a la nube
    public String alaNube(File archivo) {
        try {
            Map uploadResult = cloud.uploader().upload(archivo, ObjectUtils.asMap(
                    "resource_type", "auto"
            ));
            return uploadResult.get("url").toString();
        } catch (IOException e) {
            //Aquí renderiza un mensaje de excepción
            return null;
        }
    }

    //Inicializa el archivo Cloudinary
    static {
        cloud = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "negromano",
                "api_key", "824762479722959",
                "api_secret", "bJ1bN0Z467u0Da8bB4p_YYO96YU"));
    }

}


