package models;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Constraint;

//Modelo del Artista
@Entity
public class Artista extends Model {

    public static Finder<String, Artista> finder = new Finder<String, Artista>(Artista.class);
    //Usuario con el que se identifica al Artista, es la llave primaria
    @Id
    @Constraints.Required
    private String usuario;
    //Contraseña con la que se loguea el Artista
    @Constraints.Required
    private String pass;
    //Nombre del Artista
    @Constraints.Required
    private String nombre;
    //Edad del artista
    @Constraints.Min(0)
    private int edad;
    //Correo Electrónico de contacto
    private String correo;

    //Constructor vacío de la clase
    public Artista() {
    }

    //Constructor con únicamente el user y el pass
    public Artista(String usuario, String pass) {
        this.usuario = usuario;
        this.pass = pass;
    }

    //Constructor con todos los atributos necesarios de la clase
    public Artista(String usuario, String pass, String nombre, int edad, String correo) {
        this.usuario = usuario;
        this.pass = pass;
        this.nombre = nombre;
        this.edad = edad;
        this.correo = correo;
    }

    // Getters y Setters
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public String getCorreoElectronico() {
        return correo;
    }
    public void setCorreoElectronico(String correoElectronico) {
        this.correo = correoElectronico;
    }
}
