package models;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.annotation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;

//Modelo de la Canción
@Entity
public class Cancion extends Model {

    //Buscador en la base de datos
    public static Finder<Integer, Cancion> finder = new Finder<Integer, Cancion>(Cancion.class);
    //Identificador númerico de las canciones
    @Id
    private Integer id;
    //Usuario del artista dueño de la canción
    @NotNull
    private String artista;
    //Nombre de la canción
    @NotNull
    private String nombre;
    //Género de la canción
    private String genero;
    //Dirección URL del audio
    @NotNull
    private String direccion;

    //Constructor vacío
    public Cancion() {

    }

    
    //Constructor completo
    public Cancion(String nombre, String genero, Integer id, String artista, String direccion) {
        this.nombre = nombre;
        this.genero = genero;
        this.id = id;
        this.artista = artista;
        this.direccion = direccion;
    }


    //Getter y Setters
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getGenero() {
        return genero;
    }
    public void setGenero(String genero) {
        this.genero = genero;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getArtista() {
        return artista;
    }
    public void setArtista(String artista) {
        this.artista = artista;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
