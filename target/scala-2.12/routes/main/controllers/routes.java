
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/conf/routes
// @DATE:Mon Oct 30 11:12:56 COT 2017

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseControladorArtista ControladorArtista = new controllers.ReverseControladorArtista(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseControladorCancion ControladorCancion = new controllers.ReverseControladorCancion(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseControladorArtista ControladorArtista = new controllers.javascript.ReverseControladorArtista(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseControladorCancion ControladorCancion = new controllers.javascript.ReverseControladorCancion(RoutesPrefix.byNamePrefix());
  }

}
