
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/conf/routes
// @DATE:Mon Oct 30 11:12:56 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
