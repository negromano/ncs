
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object buscarArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Artista],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form: Form[Artista]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""

"""),_display_(/*3.2*/layout("Busca a tu artista")/*3.30*/ {_display_(Seq[Any](format.raw/*3.32*/("""
    """),format.raw/*4.5*/("""<div class="container">

    """),_display_(/*6.6*/helper/*6.12*/.form(action = helper.CSRF(routes.ControladorArtista.buscarArtista()))/*6.82*/ {_display_(Seq[Any](format.raw/*6.84*/("""
        """),_display_(/*7.10*/helper/*7.16*/.inputText(form("Usuario"))),format.raw/*7.43*/("""
        """),format.raw/*8.9*/("""<input type="submit" value="Busca a tu Artista">
    """)))}),format.raw/*9.6*/("""
    """),format.raw/*10.5*/("""</div>
""")))}))
      }
    }
  }

  def render(form:Form[Artista]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Artista]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:34 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/buscarArtista.scala.html
                  HASH: fc402731c3ffc16fffa6944810a28c2d0d22f936
                  MATRIX: 971->1|1087->22|1115->25|1151->53|1190->55|1221->60|1276->90|1290->96|1368->166|1407->168|1443->178|1457->184|1504->211|1539->220|1622->274|1654->279
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|38->6|38->6|38->6|38->6|39->7|39->7|39->7|40->8|41->9|42->10
                  -- GENERATED --
              */
          