
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object mostrarArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Artista,List[Cancion],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(artista : Artista, canciones: List[Cancion]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.47*/("""

"""),_display_(/*3.2*/layout(artista.getNombre)/*3.27*/{_display_(Seq[Any](format.raw/*3.28*/("""

    """),_display_(/*5.6*/artista/*5.13*/.getNombre),format.raw/*5.23*/("""
    """),_display_(/*6.6*/if(canciones.isEmpty)/*6.27*/{_display_(Seq[Any](format.raw/*6.28*/("""
        """),format.raw/*7.9*/("""<h2>"""),_display_(/*7.14*/artista/*7.21*/.getNombre),format.raw/*7.31*/(""" """),format.raw/*7.32*/("""No tiene canciones actualmente</h2>
    """)))}),_display_(/*8.7*/if(canciones.nonEmpty)/*8.29*/{_display_(Seq[Any](format.raw/*8.30*/("""
    """),_display_(/*9.6*/for(song <- canciones) yield /*9.28*/ {_display_(Seq[Any](format.raw/*9.30*/("""
        """),format.raw/*10.9*/("""<h4><a href=""""),_display_(/*10.23*/routes/*10.29*/.ControladorCancion.mostrarCancion(song.getId)),format.raw/*10.75*/("""">
            Cancion: """),_display_(/*11.23*/song/*11.27*/.getNombre),format.raw/*11.37*/("""
        """),format.raw/*12.9*/("""</a>
        </h4>
    """)))}),format.raw/*14.6*/("""
    """)))}),format.raw/*15.6*/("""

""")))}))
      }
    }
  }

  def render(artista:Artista,canciones:List[Cancion]): play.twirl.api.HtmlFormat.Appendable = apply(artista,canciones)

  def f:((Artista,List[Cancion]) => play.twirl.api.HtmlFormat.Appendable) = (artista,canciones) => apply(artista,canciones)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:35 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/mostrarArtista.scala.html
                  HASH: e0ac9b7945d2eb5fa4f0be05b83942615d9989d6
                  MATRIX: 980->1|1120->46|1148->49|1181->74|1219->75|1251->82|1266->89|1296->99|1327->105|1356->126|1394->127|1429->136|1460->141|1475->148|1505->158|1533->159|1603->201|1633->223|1671->224|1702->230|1739->252|1778->254|1814->263|1855->277|1870->283|1937->329|1989->354|2002->358|2033->368|2069->377|2123->401|2159->407
                  LINES: 28->1|33->1|35->3|35->3|35->3|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|42->10|42->10|42->10|42->10|43->11|43->11|43->11|44->12|46->14|47->15
                  -- GENERATED --
              */
          