
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object eliminarArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(usuario: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.19*/("""

"""),_display_(/*3.2*/layout("Eliminar")/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""
    """),format.raw/*4.5*/("""<div class="container">
        <h1 align="center"> ¿Está usted seguro de querer eliminar su cuenta permanentemente?</h1>
        <div align="center">
            <a class="btn btn-danger" href=""""),_display_(/*7.46*/routes/*7.52*/.ControladorArtista.eliminar(usuario)),format.raw/*7.89*/("""">
                Sí, deseo eliminar mi Cuenta permanentemente</a>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(usuario:String): play.twirl.api.HtmlFormat.Appendable = apply(usuario)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (usuario) => apply(usuario)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:34 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/eliminarArtista.scala.html
                  HASH: efd3593e747714a3d8e63ff575028befbccdd81a
                  MATRIX: 966->1|1078->18|1106->21|1132->39|1171->41|1202->46|1424->242|1438->248|1495->285
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|39->7|39->7|39->7
                  -- GENERATED --
              */
          