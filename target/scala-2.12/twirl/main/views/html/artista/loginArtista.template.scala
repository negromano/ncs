
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object loginArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Artista],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form: Form[Artista]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""

"""),_display_(/*3.2*/layout("Ingresa a tu Cuenta")/*3.31*/ {_display_(Seq[Any](format.raw/*3.33*/("""

    """),_display_(/*5.6*/helper/*5.12*/.form(action = helper.CSRF(routes.ControladorArtista.mostrarArtista()))/*5.83*/ {_display_(Seq[Any](format.raw/*5.85*/("""
        """),_display_(/*6.10*/helper/*6.16*/.inputText(form("Usuario"))),format.raw/*6.43*/("""
        """),_display_(/*7.10*/helper/*7.16*/.inputPassword(form("Pass"))),format.raw/*7.44*/("""

        """),format.raw/*9.9*/("""<input type="submit" value="Ingresar" class="btn btn-success">
    """)))}),format.raw/*10.6*/("""

""")))}))
      }
    }
  }

  def render(form:Form[Artista]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Artista]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:34 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/loginArtista.scala.html
                  HASH: e15ceacf5aa41471401bd3770f867062b6f29ad8
                  MATRIX: 970->1|1086->22|1114->25|1151->54|1190->56|1222->63|1236->69|1315->140|1354->142|1390->152|1404->158|1451->185|1487->195|1501->201|1549->229|1585->239|1683->307
                  LINES: 28->1|33->1|35->3|35->3|35->3|37->5|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|41->9|42->10
                  -- GENERATED --
              */
          