
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object mostrarTodos extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[Artista],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(lista: List[Artista]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.24*/("""
"""),_display_(/*2.2*/layout("Todos los Artistas")/*2.30*/ {_display_(Seq[Any](format.raw/*2.32*/("""
    """),format.raw/*3.5*/("""<h4> Esta página se hizo para mostrar todos los artistas y sus respectivas contraseñas,
    es nétamente para fines de prueba</h4>
    """),_display_(/*5.6*/for(artista <- lista) yield /*5.27*/ {_display_(Seq[Any](format.raw/*5.29*/("""
        """),format.raw/*6.9*/("""<div>
            <h2>
                Artista: """),_display_(/*8.27*/artista/*8.34*/.getNombre),format.raw/*8.44*/("""
                """),format.raw/*9.17*/("""User: """),_display_(/*9.24*/artista/*9.31*/.getUsuario),format.raw/*9.42*/("""
                """),format.raw/*10.17*/("""Pass: """),_display_(/*10.24*/artista/*10.31*/.getPass),format.raw/*10.39*/("""
            """),format.raw/*11.13*/("""</h2>
        </div>
    """)))}),format.raw/*13.6*/("""
""")))}))
      }
    }
  }

  def render(lista:List[Artista]): play.twirl.api.HtmlFormat.Appendable = apply(lista)

  def f:((List[Artista]) => play.twirl.api.HtmlFormat.Appendable) = (lista) => apply(lista)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:35 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/mostrarTodos.scala.html
                  HASH: 144d69a5155ed3daf48113c4f8ca970666d783a7
                  MATRIX: 970->1|1087->23|1114->25|1150->53|1189->55|1220->60|1381->196|1417->217|1456->219|1491->228|1566->277|1581->284|1611->294|1655->311|1688->318|1703->325|1734->336|1779->353|1813->360|1829->367|1858->375|1899->388|1955->414
                  LINES: 28->1|33->1|34->2|34->2|34->2|35->3|37->5|37->5|37->5|38->6|40->8|40->8|40->8|41->9|41->9|41->9|41->9|42->10|42->10|42->10|42->10|43->11|45->13
                  -- GENERATED --
              */
          