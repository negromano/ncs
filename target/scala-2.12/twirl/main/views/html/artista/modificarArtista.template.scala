
package views.html.artista

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object modificarArtista extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Artista],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form: Form[Artista]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""

"""),_display_(/*3.2*/layout("Modificar")/*3.21*/{_display_(Seq[Any](format.raw/*3.22*/("""

    """),_display_(/*5.6*/helper/*5.12*/.form(action = helper.CSRF(routes.ControladorArtista.actualizar()))/*5.79*/ {_display_(Seq[Any](format.raw/*5.81*/("""
        """),_display_(/*6.10*/helper/*6.16*/.inputText(form("Usuario"))),format.raw/*6.43*/("""
        """),_display_(/*7.10*/helper/*7.16*/.inputPassword(form("Pass"))),format.raw/*7.44*/("""
        """),_display_(/*8.10*/helper/*8.16*/.inputText(form("Nombre"))),format.raw/*8.42*/("""
        """),_display_(/*9.10*/helper/*9.16*/.inputText(form("Correo Electrónico"))),format.raw/*9.54*/("""
        """),_display_(/*10.10*/helper/*10.16*/.inputText(form("Edad"))),format.raw/*10.40*/("""

        """),format.raw/*12.9*/("""<input type="submit" value="Guardar Cambios" class="btn btn-success">
    """)))}),format.raw/*13.6*/("""



""")))}))
      }
    }
  }

  def render(form:Form[Artista]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Artista]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:35 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/artista/modificarArtista.scala.html
                  HASH: 90386fff4fafa738f8ca246347a34382fcae94a9
                  MATRIX: 974->1|1090->22|1118->25|1145->44|1183->45|1215->52|1229->58|1304->125|1343->127|1379->137|1393->143|1440->170|1476->180|1490->186|1538->214|1574->224|1588->230|1634->256|1670->266|1684->272|1742->310|1779->320|1794->326|1839->350|1876->360|1981->435
                  LINES: 28->1|33->1|35->3|35->3|35->3|37->5|37->5|37->5|37->5|38->6|38->6|38->6|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|42->10|42->10|42->10|44->12|45->13
                  -- GENERATED --
              */
          