
package views.html.cancion

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object agregarCancion extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[Form[Cancion],String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form: Form[Cancion], artista: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.40*/("""

"""),_display_(/*3.2*/layout("Sube tu archivo de audio")/*3.36*/ {_display_(Seq[Any](format.raw/*3.38*/("""

    """),format.raw/*5.5*/("""<div class="container">
        <div class="col-sm-4">
            """),_display_(/*7.14*/helper/*7.20*/.form(action = helper.CSRF(routes.ControladorCancion.upload(artista)), 'enctype -> "multipart/form-data")/*7.125*/ {_display_(Seq[Any](format.raw/*7.127*/("""
                """),_display_(/*8.18*/helper/*8.24*/.inputFile(form("Archivo de Audio"))),format.raw/*8.60*/("""
                """),_display_(/*9.18*/helper/*9.24*/.CSRF.formField),format.raw/*9.39*/("""
                """),format.raw/*10.17*/("""<div class="form-group">
                    <label for="usr">Nombre:</label>
                    <input type="text" name="nombre" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="usr">Género:</label>
                    <input type="text" name="genero" value="" class="form-control">
                </div>

                <input type="submit" value="Listo"/>
            """)))}),format.raw/*20.14*/("""
        """)))}),format.raw/*21.10*/("""
        """),format.raw/*22.9*/("""</div>
    </div>"""))
      }
    }
  }

  def render(form:Form[Cancion],artista:String): play.twirl.api.HtmlFormat.Appendable = apply(form,artista)

  def f:((Form[Cancion],String) => play.twirl.api.HtmlFormat.Appendable) = (form,artista) => apply(form,artista)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:35 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/cancion/agregarCancion.scala.html
                  HASH: 91ca05f6b8b585584ad41e427e78cc34e779139e
                  MATRIX: 979->1|1112->39|1140->42|1182->76|1221->78|1253->84|1347->152|1361->158|1475->263|1515->265|1559->283|1573->289|1629->325|1673->343|1687->349|1722->364|1767->381|2251->834|2292->844|2328->853
                  LINES: 28->1|33->1|35->3|35->3|35->3|37->5|39->7|39->7|39->7|39->7|40->8|40->8|40->8|41->9|41->9|41->9|42->10|52->20|53->21|54->22
                  -- GENERATED --
              */
          