
package views.html.cancion

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object consultaCancion extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Cancion,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(cancion : Cancion):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.21*/("""

"""),_display_(/*3.2*/layout(cancion.getNombre)/*3.27*/{_display_(Seq[Any](format.raw/*3.28*/("""
    """),format.raw/*4.5*/("""<h2>"""),_display_(/*4.10*/cancion/*4.17*/.getNombre),format.raw/*4.27*/("""</h2>
    <audio id="audio" class="audio-player" src=""""),_display_(/*5.50*/cancion/*5.57*/.getDireccion),format.raw/*5.70*/("""" controls>
        Tu navegador no permite reproducir audio.
    </audio>
    <h3> Género: """),_display_(/*8.19*/cancion/*8.26*/.getGenero),format.raw/*8.36*/("""</h3>

""")))}))
      }
    }
  }

  def render(cancion:Cancion): play.twirl.api.HtmlFormat.Appendable = apply(cancion)

  def f:((Cancion) => play.twirl.api.HtmlFormat.Appendable) = (cancion) => apply(cancion)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Oct 30 12:06:35 COT 2017
                  SOURCE: C:/Users/juanp/Desktop/negromano-ncsongs-juanpablo-1fee771e0695/app/views/cancion/consultaCancion.scala.html
                  HASH: e9f4f4a94c931a669cb566256506bfc54b4f582d
                  MATRIX: 967->1|1081->20|1109->23|1142->48|1180->49|1211->54|1242->59|1257->66|1287->76|1368->131|1383->138|1416->151|1535->244|1550->251|1580->261
                  LINES: 28->1|33->1|35->3|35->3|35->3|36->4|36->4|36->4|36->4|37->5|37->5|37->5|40->8|40->8|40->8
                  -- GENERATED --
              */
          