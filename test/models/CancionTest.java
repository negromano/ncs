package models;

import org.junit.Test;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;

public class CancionTest extends WithApplication {

    private Cancion cancion;

    public void setEscenario1(){
        cancion = new Cancion();
    }

    public void setEscenario2(){
        cancion = new Cancion("Black Ice","Hard Rock",1773,"ACDC","test.mp3" );
    }

    @Test
    public void testAgregacion(){

        setEscenario1();
        assertEquals("No debería haber ningún valor asignado", null , cancion.getArtista());
        assertEquals("No debería haber ningún valor asignado", null , cancion.getId());

        setEscenario2();
        assertEquals("Debería haber un valor asignado", (long)1773, (long)cancion.getId());
        assertEquals("Debería haber un valor asignado", "ACDC" , cancion.getArtista());
        assertEquals("Debería haber un valor asignado", "test.mp3" , cancion.getDireccion());
        assertEquals("Debería haber un valor asignado", "Hard Rock" , cancion.getGenero());
        assertEquals("Debería haber un valor asignado", "Black Ice" , cancion.getNombre());

    }

}